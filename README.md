# Automated Test

This repository is created for testing purposes. It contains sample code that will be used to do automated browser test of our SDC App.

## Contents

- [Getting Started](#getting-started)
- [Prerequisites](#prerequisites)

## Getting Started

These instructions will help you get a copy of the project up and running on your local machine.

### Prerequisites

List any prerequisites or requirements needed to run the software.

- Python 3.11
- Selenium
- dotenv
- a valid USERNAME and PASSWORD in .env file
