import os
import time
from selenium import webdriver
from dotenv import load_dotenv
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC

load_dotenv()
user_name = os.getenv('USERNAME')
pass_word = os.getenv('PASSWORD')

driver = webdriver.Chrome()
driver.get("https://developmentdata.dallascityhall.com/")
driver.maximize_window()


def clickable_btn(text, CSS="ID"):
    WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((getattr(By, CSS), text))
    ).click()


def find_element_by(text, CSS="ID"):
    return WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((getattr(By, CSS), text))
    )


def signsSpan():
    driver.implicitly_wait(5)
    signs_span = find_element_by(".toolbarItem > span", "CSS_SELECTOR")
    action = ActionChains(driver)
    action.move_to_element(signs_span).perform()
    driver.implicitly_wait(5)


try:
    # log in
    clickable_btn("#user-tools a", "CSS_SELECTOR")
    username = find_element_by("id_username").send_keys(user_name)
    password = find_element_by("id_password").send_keys(pass_word)
    clickable_btn("loginBtn")

    # go to sign page
    element_to_scroll = find_element_by(
        "a[href='/public_signs/']", "CSS_SELECTOR")
    driver.execute_script("arguments[0].scrollIntoView();", element_to_scroll)
    element_to_scroll.click()

    # get alcohol Variance Sign
    signsSpan()
    clickable_btn("alcoholVarianceSign")
    alcohol_variance_sign_case_no = find_element_by(
        "id_alcohol_variance_sign_case_number").send_keys('AV123-321')
    submit_alcohol_variance_btn = find_element_by(
        "submitAlcoholVarianceSignCaseNumberBtn").click()

    # get Demolition and Removal Sign
    signsSpan()
    clickable_btn("demolitionSign")
    demolition_and_removal_sign_case_no = find_element_by(
        "id_demolition_sign_case_number").send_keys('CD123-123')
    demolition_and_removal_sign_address = find_element_by(
        "id_demolition_sign_address").send_keys(
        '1500 Marilla St')
    clickable_btn("submitDemolitionSignCaseNumberBtn")

    # get SPSD Sign
    signsSpan()
    clickable_btn("spsdSign")
    zoning_sign_case_number = find_element_by(
        "id_zoning_sign_case_number").send_keys('S123-123')
    clickable_btn("submitZoningSignCaseNumberBtn")

    # get SPSD CA Sign
    signsSpan()
    clickable_btn("spsdCASign")
    spsd_ca_sign_permit_number = find_element_by(
        "id_sign_permit_number").send_keys('2210170015')
    spsd_ca_sign_posse_url = find_element_by("id_posse_url").send_keys(
        'https://developmentdata.dallascityhall.com/public_signs/createSPSDCASign/')
    spsd_ca_sign_is_attached_btn = find_element_by("id_is_attached")
    spsd_ca_sign_is_attached_select = Select(
        spsd_ca_sign_is_attached_btn)

    spsd_ca_sign_is_attached_select.select_by_index(0)
    clickable_btn("submitZoningSignCaseNumberBtn")

    spsd_ca_sign_is_attached_select.select_by_index(0)
    clickable_btn("submitZoningSignCaseNumberBtn")

    # get Proposed Rezoning Sign
    signsSpan()
    clickable_btn("propozedRezoningSign")
    proposed_rezoning_sign_case_number = find_element_by(
        "id_zoning_sign_case_number").send_keys("Z234-133")
    clickable_btn("submitZoningSignCaseNumberBtn")

    # get generic Sign
    driver.implicitly_wait(15)
    signsSpan()
    clickable_btn("genericSign")
    generic_sign_case_number = find_element_by(
        "id_zoning_sign_case_number").send_keys("S123-123")
    clickable_btn("submitZoningSignCaseNumberBtn")

    # get Demolition Delay Sign
    signsSpan()
    clickable_btn("demolitionDelaySign")
    demolition_delay_sign_address = find_element_by(
        "id_demolition_delay_sign_address").send_keys('1500 Marilla St')

    demolition_delay_sign_delay_until = find_element_by(
        "id_demolition_delay_sign_delay_until").send_keys('11/15/2023')
    submit_alcohol_variance_btn = find_element_by(
        "submitDemolitionDelaySignAddressBtn").click()

finally:
    time.sleep(3)
    driver.quit()
